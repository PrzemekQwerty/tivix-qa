from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

# WEBSITE LAUNCH
driver = webdriver.Chrome()
driver.get("http://qalab.pl.tivixlabs.com/")
driver.implicitly_wait(1)

# COUNTRY SELECTION
country = driver.find_element(By.ID, 'country')
country.click()
driver.implicitly_wait(1)
country = Select(driver.find_element(By.ID, 'country'))
country.select_by_value('1')

# CITY SELECTION
country = driver.find_element(By.ID, 'city')
country.click()
driver.implicitly_wait(1)
country = Select(driver.find_element(By.ID, 'city'))
country.select_by_value('1')

# MODEL SELECTION
model = driver.find_element(By.ID, 'model')
model.send_keys('Toyota')

# PICK-UP DATE SELECTION
model = driver.find_element(By.ID, 'pickup')
model.send_keys('29/08/2022')

# DROP-OFF DATE SELECTION
model = driver.find_element(By.ID, 'dropoff')
model.send_keys('30/08/2022')

# SEARCH BUTTON CLICK
search = driver.find_element(By.CLASS_NAME, "btn-primary")
search.click()

# RENT BUTTON CLICK
rent1 = driver.find_element(By.CLASS_NAME, "btn-success")
rent1.click()

# SECOND RENT BUTTON CLICK
rent2 = driver.find_element(By.CLASS_NAME, "btn-primary")
rent2.click()

# NAME SELECTION
name = driver.find_element(By.ID, 'name')
name.send_keys('Przemysław')

# LAST NAME SELECTION
last_name = driver.find_element(By.ID, 'last_name')
last_name.send_keys('Szpak')

# SELECTION
model = driver.find_element(By.ID, 'card_number')
model.send_keys('123123123123123')

# SELECTION
model = driver.find_element(By.ID, 'email')
model.send_keys('tivix@tivix.com')

# THIRD RENT BUTTON CLICK
rent3 = driver.find_element(By.CLASS_NAME, "btn-primary")
rent3.click()
